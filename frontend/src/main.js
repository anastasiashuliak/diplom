import Vue from 'vue'
import App from './App'
// import router from './router'
import axios from 'axios'
import App_start from './MainPage'
import router from './router/index.js';




function onAxiosResponse(response) {
  return response

}

function onAxiosError(error) {
  return error
}

Vue.config.productionTip = false

axios.defaults.baseURL = "http://localhost:8080";
axios.interceptors.response.use(onAxiosResponse,onAxiosError);

/* eslint-disable no-new */
new Vue({
  el: 'app',
  router,
  template: '<App_start/>',
  components: { App_start }
})
