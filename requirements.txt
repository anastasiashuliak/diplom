asgiref==3.3.1
certifi==2021.5.30
chardet==4.0.0
Django==3.1.7
django-cors-headers==3.7.0
djangorestframework==3.12.4
docopt==0.6.2
idna==2.10
npm==0.1.1
optional-django==0.1.0
Pillow==8.2.0
pipreqs==0.4.10
postgres==3.0.0
psycopg2==2.8.6
psycopg2-binary==2.8.6
psycopg2-pool==1.1
pytz==2021.1
requests==2.25.1
sqlparse==0.4.1
urllib3==1.26.5
vue==0.0.1
yarg==0.1.9
