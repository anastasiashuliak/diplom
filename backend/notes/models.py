from uuid import uuid1

from django.contrib.auth.models import User, Group
# Create your models here.
from django.db import models


class courses(models.Model):
    title = models.CharField("Название", max_length=25)
    price = models.FloatField("Стоимость")

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'
        db_table = 'notes_courses'

    def __str__(self):
        return self.title


class office(models.Model):
    number = models.CharField("Имя", max_length=10)

    class Meta:
        verbose_name = 'Кабинет'
        verbose_name_plural = 'Кабинеты'
        db_table = 'notes_office'

    def __str__(self):
        return self.number


class officeUser(models.Model):
    user_div_id = models.ForeignKey(User, blank=True, on_delete=models.PROTECT, verbose_name="Преподаватель")
    course_id = models.ForeignKey(courses, blank=True, on_delete=models.PROTECT, verbose_name="Курс")
    office_id = models.ForeignKey(office, blank=True, on_delete=models.PROTECT, verbose_name="Кабинет")

    class Meta:
        verbose_name = 'Курс в кабинете'
        verbose_name_plural = 'Курсы в кабинете'

    def __str__(self):
        return self.office_id


class schedule(models.Model):
    time = models.TimeField(verbose_name="Время")
    data = models.DateField(verbose_name="Дата")
    course_id = models.ForeignKey(courses, blank=True, on_delete=models.PROTECT, verbose_name="Курс")
    office_id = models.ForeignKey(office, blank=True, on_delete=models.PROTECT, verbose_name="Кабинет")

    class Meta:
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписания'

    def __str__(self):
        return self.course_id


# class group(models.Model):
#     user_div_id = models.ForeignKey(userDiv, blank=True, on_delete=models.PROTECT, verbose_name="Пользователь")
#     role_id = models.ForeignKey(role, blank=True, on_delete=models.PROTECT, verbose_name="Роль")
#
#     class Meta:
#         verbose_name = 'Группа'
#         verbose_name_plural = 'Группы'
#
#     def __str__(self):
#         return self.user_div_id
