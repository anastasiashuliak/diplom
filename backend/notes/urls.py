from rest_framework import routers
from .views import *
from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter


router = DefaultRouter()

# router = routers.DefaultRouter()
router.register(r'notes', OfficeViewSet)
# urlpatterns = router.urls
urlpatterns = [
    path(r'', index, name='index'),
    path(r'info/', include(router.urls)),


]
