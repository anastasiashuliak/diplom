from django.shortcuts import render
from django.http import JsonResponse, HttpResponseServerError, HttpResponse
# Create your views here.
from rest_framework import viewsets

from .serializers import OfficeSerializer
from .models import office
#from.models import Note


def index(request):
    # from django.middleware.csrf import get_token
    # csrf_token = get_token(request)
    context = {
        # 'data': 'value',
        # 'csrf_token':request.META.get('CSRF_COOKIE'),
    }
    request.path = '/'
    rend = render(request, 'index.html', context)

    return rend

class OfficeViewSet (viewsets.ModelViewSet):
    queryset = office.objects.all().order_by('number')
    serializer_class = OfficeSerializer
